<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Mascota;

class DatabaseSeeder extends Seeder
{

	private $arrayMascotas = array(
		array(
			'nombre' => 'Kira',
			'especie' => 'Perro',
			'raza' => 'Galgo',
			'fechaNacimiento' => '2014-09-07',
			'imagen' => 'kira.jpg',
			'historial' => 'Vacunas al día',
			'cliente' => 'Iván'
		),
		array(
			'nombre' => 'Conejo',
			'especie' => 'Conejo',
			'raza' => 'Enano',
			'fechaNacimiento' => '2011-07-07',
			'imagen' => 'conejo.jpg',
			'historial' => 'Limado de dientes. Vacunas al día',
			'cliente' => 'Pepita'
		),
		array(
			'nombre' => 'Gato',
			'especie' => 'Gato',
			'raza' => 'Siamés',
			'fechaNacimiento' => '2016-05-16',
			'imagen' => 'gato.jpg',
			'historial' => 'Todo bien',
			'cliente' => 'Pepe'
		),
	);

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        self::seedMascotas();
 		$this->command->info('Tabla mascotas inicializada con datos');

        self::seedUsers();
        $this->command->info('Tabla usuarios inicializada con datos');
    }


    public function seedMascotas()
    {
    	DB::table('mascotas')->delete();

    	foreach ($this->arrayMascotas as $mascota)
		{
			$m = new Mascota();
			$m->nombre = $mascota['nombre'];
			$m->especie = $mascota['especie'];
			$m->raza = $mascota['raza'];
			$m->fechaNacimiento = $mascota['fechaNacimiento'];
			$m->imagen = $mascota['imagen'];
			$m->historial = $mascota['historial'];
			$m->cliente = $mascota['cliente'];
			$m->save();
		}
    }

    public function seedUsers()
    {
    	DB::table('users')->delete();

		$u = new User();
		$u->name = "alvaro";
		$u->email = "alvaro10g@hotmail.com";
		$u->password = bcrypt(1234);
		$u->save();

		$u = new User();
		$u->name = "admin";
		$u->email = "admin@admin.com";
		$u->password = bcrypt('admin');
		$u->save();		
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mascota;
use Illuminate\Support\Str;

class RestWebServiceController extends Controller
{
    //

    public function getTodas()
    {
        $mascotas = Mascota::all();
        return response()->json($mascotas);
    }

    public function getVer($id)
    {       
        $mascota = Mascota::where('id', $id)->first();
        var_dump($mascota);
        return response()->json($mascota);
    }

    public function insertar(Request $Request)
    {
        $mascota = new Mascota(); 
        if ($Request->has('nombre') && $Request->has('especie') && $Request->has('raza') && $Request->has('fechaNacimiento') && $Request->has('imagen') && $Request->has('historial') && $Request->has('cliente')) {
            
            $mascota->nombre = $Request->nombre;
            $mascota->slug = Str::slug($Request->nombre);
            $mascota->especie = $Request->especie;
            $mascota->raza = $Request->raza;
            $mascota->fechaNacimiento = $Request->fechaNacimiento;
            $mascota->imagen = $Request->imagen;
            $mascota->historial = $Request->historial; 
            $mascota->cliente = $Request->cliente;

            $mascota->save();

             return response()->json(['mensaje' => "Mascota insertada correctamente"]);
        }else{

            return response()->json(['mensaje' => "Error al insertar la mascota"]);
        }
    }

    public function borrar($id)
    {
        $mascota = Mascota::where('id', $id)->first();
        $mascota->destroy();
    }
}
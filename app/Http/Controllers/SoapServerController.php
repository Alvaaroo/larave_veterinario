<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapServer;
use App\lib\WSDLDocument;
use Auth;
use App\Mascota;

class SoapServerController extends Controller
{
    private $clase = "\\App\\Http\\Controllers\\VeterinarioWebService";
    private $uri = "http://veterinario.es/api";
    private $servidor = "http://veterinario.es/api";
    private $UrlWSDL = "http://veterinario.es/api/wsdl";

    public function getServer()
    {
        $server = new SoapServer($this->UrlWSDL);
        $server->setClass($this->clase);
        $server->handle();
        exit();
    }

    public function getWSDL()
    {
        $wsdl = new WSDLDocument($this->clase, $this->servidor, $this->uri);
        $wsdl->formatOutput = true;
        header('Content-Type: text/xml');
        echo $wsdl->saveXML();
        exit();
    }
}

class VeterinarioWebService
{

    /**
     * Funcion de login
     * @param string $email 
     * @param string $password 
     * @return boolean
     */
    public function login($email, $password)
    {
        return Auth::attempt(['email' => $email, 'password' => $password]);
    }

    /**
     * Funcion que devuelve una mascota
     * @param integer $id 
     * @return App\Mascota
     */    
    public function getMascota($id)
    {
        return Mascota::find($id);        
    }

    /**
     * Funcion que nos devuelve un array de todas las mascotas
     * @return App\Mascota[]
     */
    
    public function getMascotas()
    {
        return Mascota::all();
    }

    /**
     * Funcion que nos devuelve un array de todas las mascotas del cliente con nombre pasado por parametro
     * @param string $cliente 
     * @return App\Mascota[]
     */

    public function getMascotasCliente($cliente)
    {
        return Mascota::where('cliente', $cliente)->orderBy('nombre')->get();
    }

    /**
     * Funcion busquedad que nos busca todas las mascotas que contenga en su nombre una cadena pasada por parametro
     * @param string $nombre 
     * @return App\Mascota[]
     */

    public function busqueda($nombreMascota)
    {
        return Mascota::where('nombre', 'like','%'.$nombreMascota.'%')->get();
    }
}

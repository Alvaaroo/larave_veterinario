<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Storage;
use Illuminate\Support\Facades\Storage;
use App\Mascota;

class MascotasController extends Controller
{
    //
 //    private $arrayMascotas = array(
	// 	array(
	// 		'nombre' => 'Kira',
	// 		'especie' => 'Perro',
	// 		'raza' => 'Galgo',
	// 		'fechaNacimiento' => '2014-09-07',
	// 		'imagen' => 'kira.jpg',
	// 		'historial' => 'Vacunas al día',
	// 		'cliente' => 'Iván'
	// 	),
	// 	array(
	// 		'nombre' => 'Conejo',
	// 		'especie' => 'Conejo',
	// 		'raza' => 'Enano',
	// 		'fechaNacimiento' => '2011-07-07',
	// 		'imagen' => 'conejo.jpg',
	// 		'historial' => 'Limado de dientes. Vacunas al día',
	// 		'cliente' => 'Pepita'
	// 	),
	// 	array(
	// 		'nombre' => 'Gato',
	// 		'especie' => 'Gato',
	// 		'raza' => 'Siamés',
	// 		'fechaNacimiento' => '2016-05-16',
	// 		'imagen' => 'gato.jpg',
	// 		'historial' => 'Todo bien',
	// 		'cliente' => 'Pepe'
	// 	),
	// );    

	public function getTodas()
	{
		$mascotas = Mascota::all();
		return view('mascotas.index' , array("Mascotas" => $mascotas));
	}

	public function getVer($id)
	{    	
		$mascota = Mascota::findOrFail($id);
		return view('mascotas.mostrar', array('Mascota' => $mascota));
	}

	public function getCrear()
	{
		return view('mascotas.crear');
	}

	public function postCrear(Request $Request)
	{
		$mascota = new Mascota(); 
		if ($Request->has('nombre') && $Request->has('especie') && $Request->has('raza') && $Request->has('fechaNacimiento') && $Request->has('imagen') && $Request->has('historial') && $Request->has('cliente')) {
			
			$mascota->nombre = $Request->nombre;
			$mascota->especie = $Request->especie;
			$mascota->raza = $Request->raza;
			$mascota->fechaNacimiento = $Request->fechaNacimiento;
			$mascota->imagen = $Request->imagen->store('', 'mascotas');
			$mascota->historial = $Request->historial; 
			$mascota->cliente = $Request->cliente;
		}else{

			return redirect('mascotas/crear')->withInput();
		}
		
		$mascota->save();

		return redirect('mascotas')/*->with("mensaje", "Mascota creada correctamente")*/;
	}


	public function getEditar($id)
	{		
		$mascota = Mascota::findOrFail($id);
		return view('mascotas.editar', array('Mascota' => $mascota, "id" => $id));
	}
	public function postEditar(Request $Request)
	{
		$mascota = Mascota::findOrFail($Request->id);

		$mascota->nombre = $Request->nombre;
		$mascota->especie = $Request->especie;
		$mascota->raza = $Request->raza;
		$mascota->fechaNacimiento = $Request->fechaNacimiento;

		if ($Request->has('imagen')){

			Storage::disk('mascotas')->delete($mascota->imagen);
			$mascota->imagen = $Request->imagen->store('', 'mascotas');

		} 
		$mascota->historial = $Request->historial; 
		$mascota->cliente = $Request->cliente;
		$mascota->save();

		return redirect('mascotas')/*->with("mensaje", "Mascota editada correctamente")*/;
	}

	public function Buscar(Request $request)
	{
		$busqueda = $request->busqueda;
		$mascotas = Mascota::select("nombre")->where('nombre', 'like', "%".$busqueda."%")->plunk('nombre');

		return response()->json($mascotas);
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
* @property string $nombre
* @property string $especie
* @property string $raza
* @property string $fechaNacimiento
*/
class Mascota extends Model
{
    //
	protected $table = 'mascotas';
	protected $primaryKey = 'id';
	public $timestamps = false;


	public function getEdad()
	{
		$fechaFormateada = Carbon::parse($this->fechaNacimiento);
		return $fechaFormateada->diffInYears(Carbon::now());
	}

}

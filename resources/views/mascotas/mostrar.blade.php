@extends('layouts.master')
@section('titulo')
mostrar Mascota
@endsection
@section('contenido')
<div class="row">
	{{-- @foreach($Mascota as $valor)	 --}}
	{{-- @if($id == $Mascota->id) --}}
	<div class="col-sm-3">
		{{-- TODO: Imagen de la mascota --}}
		<img src="http://localhost/DWES/laravel_veterinario/public/assets/imagenes/{{$Mascota->imagen}}" class="fluid" height="400" width="400" />	
	</div>
	<div class="col-sm-9">
		{{-- TODO: Datos de la mascota --}}
		<h2 style="min-height:45px;margin:5px 0 10px 0">{{$Mascota->nombre}}</h2>
		<h5>Edad:</h5>
		<p>
			@if($Mascota->getEdad() == 1)
			{{$Mascota->getEdad()}} año
			@endif

			@if($Mascota->getEdad() != 1)
			{{$Mascota->getEdad()}} años
			@endif
		</p>
		<h5>Raza</h5>
		<p>{{$Mascota->raza}}</p>
		<h5>Cliente</h5>
		<p>{{$Mascota->cliente}}</p>
		<h5>Historial</h5>
		<p>{{$Mascota->historial}}</p>
		<a class="btn btn-warning" href="{{ url('/mascotas/editar/' . $Mascota->id ) }}"><img src="http://localhost/DWES/laravel_veterinario/public/assets/imagenes/editar.png" height="20" width="20">Editar</a>
		<a class="btn btn-light" href="{{ action('inicioController@getInicio')}}"><img src="http://localhost/DWES/laravel_veterinario/public/assets/imagenes/volver.png" height="20" width="20">Volver al listado</a>
	</div>
</div>
{{-- @endif --}}
{{-- @endforeach --}}
@endsection
@extends('layouts.master')
@section('titulo')
Veterinario (index)
@endsection
@section('contenido')
{{-- <div class="container"> --}}
	<div class="row">
		@foreach($Mascotas as $clave => $mascota)
		<div style="border-left:1px solid black;" class="col-xs-12 {{-- col-sm-1 --}} col-md-6">
			<img src="assets/imagenes/{{$mascota->imagen}}" class='fluid' class="card-img-bottom" height="300" width="400" margin='10' />
				<a href="{{ url('/mascotas/ver/' . $mascota->id ) }}">
					<h4 style="min-height:45px;margin:5px 0 10px 0">
						{{$mascota->nombre}}
					</h4>
				</a>
				<p>raza: {{$mascota->raza }}</p>	
				<p>cliente: {{$mascota->cliente }}</p>				
		</div>
		@endforeach
	</div>
{{-- </div> --}}
@endsection
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="{{url('/')}}">Veterinario</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      {{--@if(Auth::check() )--}}
      
      <li class="nav-item">
        <a href="{{url('/mascotas')}}" class="nav-link {{ Request::is('mascotas*') && !Request::is('mascotas/crear')? ' active' : ''}}">Listado de mascotas</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/mascotas/crear')}}" class="nav-link {{ Request::is('mascotas/crear')? ' active' : ''}}">Nueva mascota</a>
      </li>
    </ul>
    {{--@endif --}}
    @if(Auth::check() )
    <form class="form-inline mt-2 mt-md-0">
      <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
    </form>
    {{-- <script>
      $(document).ready(function() {   

        $("#busqueda" ).autocomplete({
          source: function(request, result) {
            $.ajax({
              url: "{{ url('mascotas/busquedaAjax') }}",
              // url: "busquedaAjax",
              dataType: "json",
              data: {"_token": "{{ csrf_token() }}", "busqueda": request['term']},
              success: function(data) {
                result(data);
              }
            });
          },
          position: {
            my: "left+0 top+8",
          }
        });
      });
    </script> --}}
    <script>

      $(document).ready(function() {

        $('#busqueda').autocomplete({

          source: 

          function (query, result) 

          {

            $.ajax({

              type: "POST",

              url: "{{url('busquedaAjax')}}",

              dataType: "json",

              data: {"_token": "{{ csrf_token() }}", "busqueda": query['term']},

              success : function(data){

               result(data);

             }

           });

          },

          position: {

                  my: "left+0 top+8", //Para mover la caja 8px abajo

                }

              });

      });

    </script>
    <ul class="navbar-nav navbar-right">
      <li class="nav-item">
        <a href="{{ route('logout') }}"  class="nav-link"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();" >
        <span class="glyphicon glyphicon-off"></span>
        Cerrar sesión
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </li>
  </ul>
  @else
  <ul class="navbar-nav navbar-right">
    <li class="nav-item">
      <a href="{{url('login')}}" class="nav-link">Login</a>
    </li>
  </ul>
  @endif 
</div>
</nav>







<?php

use App\Mascota;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'inicioController@getInicio');

// Route::get('login', function () {
//     return view("auth.login");
// });

// Route::get('logout', function () {
//     return "Logout usuario";
// });

Route::get('mascotas', 'MascotasController@getTodas');

Route::get('mascotas/ver/{id}', 'MascotasController@getVer')->where('id', '[0-9]+');

Route::get('mascotas/crear', 'MascotasController@getCrear');

Route::get('mascotas/editar/{id}', 'MascotasController@getEditar')->where('id', '[0-9]+');

Route::post('mascotas/crear', 'MascotasController@postCrear')->where('id', '[0-9]+');

Route::post('mascotas/editar/{id}', 'MascotasController@postEditar')->where('id', '[0-9]+');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
// Route::get('mascotas/ver/{id}', 'MascotasController@getVer')->where('id', '[0-9]+');
Route::get('mascotas/crear', 'MascotasController@getCrear');
Route::get('mascotas/editar/{id}', 'MascotasController@getEditar')->where('id', '[0-9]+');
// ...
});

// SOAP
Route::any('api', 'SoapServerController@getServer');

Route::any('api/wsdl', 'SoapServerController@getWSDL');

// REST
Route::get('rest', 'RestWebServiceController@getTodas');

Route::get('rest/{$id}', 'RestWebServiceController@getVer');

Route::delete('rest/borrar/{id}', 'RestWebServiceController@borrar');

Route::post('rest/insertar', 'RestWebServiceController@insertar');

Route::post('busquedaAjax', "MascotasController@Buscar");